"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.string.match.js");

require("core-js/modules/es.string.split.js");

require("core-js/modules/es.string.search.js");

var _react = _interopRequireWildcard(require("react"));

var _reactBootstrap = require("react-bootstrap");

var _userProfile = _interopRequireDefault(require("./userProfile"));

var _utilities = require("../Services/utilities");

var _queryString = _interopRequireDefault(require("query-string"));

var _leftArrowWhite = _interopRequireDefault(require("../assets/images/icons/left-arrow-white.svg"));

var _lodash = _interopRequireDefault(require("lodash"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const FEEDIO_UI_URL = process.env.REACT_APP_FEEDIO_UI_BASE_URL;
const CONTENTBASE_UI_URL = process.env.REACT_APP_CONTENTBASE_UI_BASE_URL;
const HITMAN_URL = process.env.REACT_APP_HITMAN_UI_BASE_URL;
const HTTPDUMP_URL = process.env.REACT_APP_HTTPDUMP_UI_BASE_URL;

class NavBar extends _react.Component {
  constructor() {
    super(...arguments);

    _defineProperty(this, "state", {
      firstName: "",
      lastName: "",
      email: ""
    });
  }

  componentDidMount() {}

  navigateToProjectList() {
    this.props.history.push({
      pathname: "/orgs/".concat(this.props.organizationId, "/projects")
    });
  }

  navigateToHome() {
    if (!_lodash.default.isEmpty(this.props.organizations)) {
      const orgId = this.props.organizationId;
      let pathname = "/orgs/".concat(orgId, "/ebl");
      let consumedServices = (0, _utilities.getConsumedServices)(this.props.organizations, orgId);

      if (consumedServices.includes("ebl")) {
        pathname = "/orgs/".concat(orgId, "/projects");
      }

      this.props.history.push({
        pathname
      });
    }
  }

  navigateToDashboard() {
    let projectId = this.props.match.params.id;
    this.props.history.push({
      pathname: "/orgs/".concat(this.props.organizationId, "/projects/").concat(projectId)
    });
  }

  navigateToProductDashboard() {
    let projectId = this.props.match.params.id;
    this.props.history.push({
      pathname: "/orgs/".concat(this.props.organizationId, "/products/")
    });
  }

  renderSearchDropdown() {
    // const pathname = this.props.location.pathname;
    // return (
    //   pathname.includes("/projects") &&
    //   <GlobalSearch organizationId={this.props.organizationId} />
    // );
    return "hello";
  }

  renderNavbarBrand() {
    const pathname = this.props.location.pathname;
    const socketIcon = process.env.REACT_APP_SOCKET_ICON;
    const socketLogo = process.env.REACT_APP_SOCKET_LOGO;
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_reactBootstrap.Navbar.Brand, {
      className: "socket-navbar"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "navbar-logo"
    }, /*#__PURE__*/_react.default.createElement("img", {
      style: {
        cursor: "pointer"
      },
      src: pathname.includes("/products") ? socketLogo : socketIcon,
      alt: "viasocket-icon",
      width: "22px",
      height: "auto",
      onClick: () => this.navigateToHome()
    }))));
  }

  renderBillingNavbar() {
    // return(
    //   this.props.billingsNavTabs && (
    //     <Navbar
    //       id="function-nav"
    //       collapseOnSelect
    //       expand="lg"
    //       variant="light"
    //       className="px-0 socket-navbar"
    //     >
    //       <React.Fragment>
    //         <Navbar.Collapse className="justify-content-center">
    //           <BillingNavBar {...this.props}/>
    //         </Navbar.Collapse>
    //         <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    //       </React.Fragment>
    //     </Navbar>
    //   )
    // )
    return "Hello Billing";
  }

  renderNavigationTabs() {
    // return (
    //   <>
    //     {this.props.showFunctionTabs && (
    //       <Navbar
    //         id="function-nav"
    //         collapseOnSelect
    //         expand="lg"
    //         variant="light"
    //         className="px-0 socket-navbar"
    //       >
    //         <React.Fragment>
    //           <Navbar.Collapse className="justify-content-center">
    //             <NavbarTabs {...this.props}></NavbarTabs>
    //           </Navbar.Collapse>
    //           <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    //         </React.Fragment>
    //       </Navbar>
    //     )}
    //     {this.props.showOrgTabs && (
    //       <Navbar
    //         id="function-nav"
    //         collapseOnSelect
    //         expand="lg"
    //         variant="light"
    //         className="px-0 socket-navbar"
    //       >
    //         <React.Fragment>
    //           <Navbar.Collapse className="justify-content-center">
    //             <SettingsNavbarTabs {...this.props}></SettingsNavbarTabs>
    //           </Navbar.Collapse>
    //           <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    //         </React.Fragment>
    //       </Navbar>
    //     )}
    //     {this.renderBillingNavbar()}
    //   </>
    // );
    return "Middle";
  }

  renderBreadcrumb() {
    // return (
    //   <Breadcrumb {...this.props} />
    // )
    return "Breadcurmb";
  }

  switchProduct(product) {
    let orgId = this.props.organizationId;
    let link;

    if (product === 'feedio') {
      link = "".concat(FEEDIO_UI_URL, "/orgs/").concat(orgId);
    } else if (product === 'hitman') {
      link = "".concat(HITMAN_URL, "/orgs/").concat(orgId);
    } else if (product === 'contentbase') {
      link = "".concat(CONTENTBASE_UI_URL, "/orgs/").concat(orgId, "/projects");
    } else if (product === 'httpdump') {
      link = "".concat(HTTPDUMP_URL);
    }

    window.open(link, '_blank');
  }

  renderNewBreadcrumb() {
    let projectId = this.props.match.params.id;
    let projectName = this.props.projects[projectId].name;
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "custom-breadcrumb d-flex"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "link",
      onClick: () => {
        this.navigateToHome();
      }
    }, " EBL "), projectName && /*#__PURE__*/_react.default.createElement("div", {
      className: "mx-1"
    }, " ", '>', " "), projectName && /*#__PURE__*/_react.default.createElement("div", {
      className: "link",
      onClick: () => {
        this.navigateToHome();
      }
    }, projectName));
  }

  renderProjectName() {
    let projectId = this.props.match.params.id;
    let projectName = this.props.projects[projectId].name;
    return projectId && /*#__PURE__*/_react.default.createElement("div", {
      className: "link-new d-flex align-items-center",
      onClick: () => {
        this.navigateToHome();
      }
    }, /*#__PURE__*/_react.default.createElement("img", {
      src: _leftArrowWhite.default,
      className: "rotate-90 mr-10"
    }), projectName);
  }

  renderManageNavbarHeading() {
    let pathArray = this.props.location.pathname.split('/');
    return this.props.manageHeading && pathArray[3] === "manage" && /*#__PURE__*/_react.default.createElement("div", {
      className: "tabs-wrapper manage-header-text"
    }, pathArray[4] === "authkeys" ? "AuthKeys" : "Manage Team");
  }

  renderBackOption() {
    let pathArray = this.props.location.pathname.split('/');
    return (pathArray[3] === "manage" || pathArray[3] === "billing") && /*#__PURE__*/_react.default.createElement("div", {
      className: "link-new d-flex align-items-center",
      onClick: () => this.props.history.goBack()
    }, /*#__PURE__*/_react.default.createElement("img", {
      src: _leftArrowWhite.default,
      className: "rotate-90 mr-10"
    }), " Back");
  }

  render() {
    console.log(this.props);
    const pathname = this.props.location.pathname;
    this.product = _queryString.default.parse(this.props.location.search).product;
    let isProjectDashboard = pathname.includes("/products");
    return /*#__PURE__*/_react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement("div", {
      className: ['headerNavbar', isProjectDashboard ? 'headerNavbarWhite' : ''].join(" ")
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "d-flex justify-content-between"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "logoContainer d-flex align-items-center"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "brand-block justify-content-left align-items-center"
    }, this.props.showDashboardButton && /*#__PURE__*/_react.default.createElement("div", {
      className: "show-dashboard text-white d-flex justify-content-center ",
      onClick: () => {
        this.navigateToProductDashboard();
      }
    }, /*#__PURE__*/_react.default.createElement("img", {
      height: '16px',
      width: '16px',
      src: _leftArrowWhite.default,
      alt: "",
      class: "mr-10 rotate-270"
    }), "Go to Product Dashboard"), !isProjectDashboard && /*#__PURE__*/_react.default.createElement("div", {
      className: "switchPrd m-1r"
    }, !this.props.showDashboardButton && /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown, null, /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Toggle, {
      variant: "success",
      id: "dropdown-basic",
      className: "d-flex align-items-center"
    }, !this.product && this.renderNavbarBrand(), " EBL ", /*#__PURE__*/_react.default.createElement("svg", {
      className: "transition ml-1",
      width: "10",
      height: "6",
      viewBox: "0 0 10 6",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, /*#__PURE__*/_react.default.createElement("path", {
      d: "M1 1L5 5L9 1",
      stroke: "#EEEEEE",
      "stroke-width": "2",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }))), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Menu, null, /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, {
      href: "#",
      className: "dropHeader"
    }, "Switch to"), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, {
      href: "#",
      onClick: () => {
        this.switchProduct('feedio');
      }
    }, "Feedio"), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, {
      href: "#",
      onClick: () => {
        this.switchProduct('hitman');
      }
    }, "Hitman"), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, {
      href: "",
      onClick: () => {
        this.switchProduct('contentbase');
      }
    }, "ContentBase"), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, {
      href: "",
      onClick: () => {
        this.switchProduct('httpdump');
      }
    }, "Httpdump"), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, {
      href: "",
      onClick: () => {
        this.navigateToProductDashboard();
      },
      className: "go-to-link"
    }, /*#__PURE__*/_react.default.createElement("img", {
      height: '16px',
      width: '16px',
      src: _leftArrowWhite.default,
      alt: "",
      class: "mr-10 rotate-270"
    }), "Go to Product dashboard"))))), !this.product && this.props.showBreadCrumb && this.renderProjectName(), this.renderBackOption()), /*#__PURE__*/_react.default.createElement("div", {
      className: "d-flex"
    }, this.renderManageNavbarHeading()), /*#__PURE__*/_react.default.createElement("div", {
      className: "d-flex justify-content-end logoContainer"
    }, this.renderSearchDropdown(), /*#__PURE__*/_react.default.createElement(_userProfile.default, this.props)))));
  }

}

var _default = NavBar;
exports.default = _default;