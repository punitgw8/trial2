import React, { Component } from "react";
import { Navbar, Button, Dropdown } from "react-bootstrap";
//import "./main.scss";
import UserProfile from "./userProfile";
import { getConsumedServices } from "../Services/utilities";
import queryString from "query-string";
//import BillingNavBar from "../billing/billingNavbar/billingNavbar";
import arrow from '../assets/images/icons/left-arrow-white.svg';
import arrowLeft from '../assets/images/icons/left-arrow-white.svg';
import _ from 'lodash';

const FEEDIO_UI_URL = process.env.REACT_APP_FEEDIO_UI_BASE_URL
const CONTENTBASE_UI_URL = process.env.REACT_APP_CONTENTBASE_UI_BASE_URL
const HITMAN_URL = process.env.REACT_APP_HITMAN_UI_BASE_URL
const HTTPDUMP_URL = process.env.REACT_APP_HTTPDUMP_UI_BASE_URL

class NavBar extends Component {
  state = {
    firstName: "",
    lastName: "",
    email: "",
  };

  componentDidMount() {}

  navigateToProjectList() {
    this.props.history.push({
      pathname: `/orgs/${this.props.organizationId}/projects`,
    });
  }

  navigateToHome() {
    if(!_.isEmpty(this.props.organizations)){
      const orgId = this.props.organizationId
      let pathname = `/orgs/${orgId}/ebl`
      let consumedServices = getConsumedServices(this.props.organizations,orgId)
      if (consumedServices.includes("ebl")) {
        pathname = `/orgs/${orgId}/projects`
      }
      this.props.history.push({
        pathname,
      });
    }
  }

  navigateToDashboard() {
    let projectId = this.props.match.params.id;
    this.props.history.push({
      pathname: `/orgs/${this.props.organizationId}/projects/${projectId}`,
    });
  }

  navigateToProductDashboard() {
    let projectId = this.props.match.params.id;
    this.props.history.push({
      pathname: `/orgs/${this.props.organizationId}/products/`,
    });
  }  

  renderSearchDropdown() {
    // const pathname = this.props.location.pathname;
    // return (
    //   pathname.includes("/projects") &&
    //   <GlobalSearch organizationId={this.props.organizationId} />
    // );
    return "hello";
  }

  renderNavbarBrand() {
    const pathname = this.props.location.pathname;
    const socketIcon = process.env.REACT_APP_SOCKET_ICON;
    const socketLogo = process.env.REACT_APP_SOCKET_LOGO;
    return (
       <React.Fragment>
        <Navbar.Brand
          className="socket-navbar"
        >
          <div className="navbar-logo">
            <img
              style={{ cursor: "pointer" }}
              src={ pathname.includes("/products") ? socketLogo : socketIcon }
              alt="viasocket-icon"
              width="22px"
              height="auto"
              onClick={() => this.navigateToHome()}
            />
          </div>
        </Navbar.Brand>
      </React.Fragment>
    );
  }

  renderBillingNavbar(){
    // return(
    //   this.props.billingsNavTabs && (
    //     <Navbar
    //       id="function-nav"
    //       collapseOnSelect
    //       expand="lg"
    //       variant="light"
    //       className="px-0 socket-navbar"
    //     >
    //       <React.Fragment>
    //         <Navbar.Collapse className="justify-content-center">
    //           <BillingNavBar {...this.props}/>
    //         </Navbar.Collapse>
    //         <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    //       </React.Fragment>
    //     </Navbar>
    //   )
    // )
    return "Hello Billing"
  }

  renderNavigationTabs() {
    // return (
    //   <>
    //     {this.props.showFunctionTabs && (
    //       <Navbar
    //         id="function-nav"
    //         collapseOnSelect
    //         expand="lg"
    //         variant="light"
    //         className="px-0 socket-navbar"
    //       >
    //         <React.Fragment>
    //           <Navbar.Collapse className="justify-content-center">
    //             <NavbarTabs {...this.props}></NavbarTabs>
    //           </Navbar.Collapse>
    //           <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    //         </React.Fragment>
    //       </Navbar>
    //     )}
    //     {this.props.showOrgTabs && (
    //       <Navbar
    //         id="function-nav"
    //         collapseOnSelect
    //         expand="lg"
    //         variant="light"
    //         className="px-0 socket-navbar"
    //       >
    //         <React.Fragment>
    //           <Navbar.Collapse className="justify-content-center">
    //             <SettingsNavbarTabs {...this.props}></SettingsNavbarTabs>
    //           </Navbar.Collapse>
    //           <Navbar.Toggle aria-controls="responsive-navbar-nav" />
    //         </React.Fragment>
    //       </Navbar>
    //     )}
    //     {this.renderBillingNavbar()}
    //   </>
    // );
    return "Middle";
  }

  renderBreadcrumb() {
    // return (
    //   <Breadcrumb {...this.props} />
    // )
    return "Breadcurmb";
  }


  switchProduct(product){
    let orgId = this.props.organizationId;
    let link;
    if(product==='feedio'){
     link=`${FEEDIO_UI_URL}/orgs/${orgId}`
    }
    else if(product==='hitman'){
      link = `${HITMAN_URL}/orgs/${orgId}`
    }
    else if(product==='contentbase'){
      link = `${CONTENTBASE_UI_URL}/orgs/${orgId}/projects`
    }
    else if(product==='httpdump'){
      link = `${HTTPDUMP_URL}`
    }
    window.open(link,'_blank');
  }

  renderNewBreadcrumb() {
    let projectId = this.props.match.params.id;
    let projectName = this.props.projects[projectId].name;
    return (
      <div className="custom-breadcrumb d-flex">
        <div className="link" onClick={() => { this.navigateToHome()} }> EBL </div>
        {projectName && <div className="mx-1"> { '>' } </div> }
        {projectName && <div className="link" onClick={() => { this.navigateToHome()}}>{projectName}</div>}
      </div>
    )
  }
  renderProjectName() {
    let projectId = this.props.match.params.id;
    let projectName = this.props.projects[projectId].name;
    return (
      projectId && <div  className="link-new d-flex align-items-center" onClick={() => { this.navigateToHome()} }> 
        <img src={arrowLeft} className="rotate-90 mr-10" />
        { projectName}
      </div> 
    )
  }

  renderManageNavbarHeading(){
    let pathArray = this.props.location.pathname.split('/');
    return(
      this.props.manageHeading && pathArray[3]==="manage" && <div className="tabs-wrapper manage-header-text">
        {pathArray[4]==="authkeys" ? "AuthKeys" : "Manage Team"}
      </div>
    )
  }

  renderBackOption(){
    let pathArray = this.props.location.pathname.split('/');
    return(
      (pathArray[3]==="manage"|| pathArray[3]==="billing") && <div className="link-new d-flex align-items-center" onClick={() => this.props.history.goBack() }> 
        <img src={arrowLeft} className="rotate-90 mr-10" />
        {" Back"}
      </div> 
    )
  }

  render() {
    console.log(this.props);
    const pathname = this.props.location.pathname;
    this.product = queryString.parse(this.props.location.search).product
    let isProjectDashboard = pathname.includes("/products")
    return (
      <React.Fragment>
        <div className={['headerNavbar',isProjectDashboard? 'headerNavbarWhite' : ''].join(" ")}>
          <div className="d-flex justify-content-between">
            <div className="logoContainer d-flex align-items-center">
              <div className="brand-block justify-content-left align-items-center">
              {this.props.showDashboardButton&&<div className='show-dashboard text-white d-flex justify-content-center ' onClick={() => { this.navigateToProductDashboard()}}><img height={'16px'} width={'16px'} src={arrow} alt='' class="mr-10 rotate-270" />Go to Product Dashboard</div>}
              {/* {!this.product && this.props.showBreadCrumb && this.renderNewBreadcrumb()} */}
              {
              !isProjectDashboard && <div className='switchPrd m-1r'>
                        {!this.props.showDashboardButton&&<Dropdown>
                          <Dropdown.Toggle variant='success' id='dropdown-basic' className="d-flex align-items-center">
                          {!this.product && this.renderNavbarBrand()} EBL <svg className="transition ml-1" width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1 1L5 5L9 1" stroke="#EEEEEE" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/></svg>
                          </Dropdown.Toggle>

                          <Dropdown.Menu>
                            <Dropdown.Item href='#' className='dropHeader'>
                              Switch to
                            </Dropdown.Item>
                            <Dropdown.Item href='#' onClick={() => { this.switchProduct('feedio')}}>
                              Feedio
                            </Dropdown.Item>
                            <Dropdown.Item href='#' onClick={() => { this.switchProduct('hitman') }}>
                              Hitman
                            </Dropdown.Item>
                            <Dropdown.Item href='' onClick={() => { this.switchProduct('contentbase') }}>
                              ContentBase
                            </Dropdown.Item>
                            <Dropdown.Item href='' onClick={() => { this.switchProduct('httpdump') }}>
                              Httpdump
                            </Dropdown.Item>
                            <Dropdown.Item href='' onClick={() => { this.navigateToProductDashboard() }} className="go-to-link">
                              <img height={'16px'} width={'16px'} src={arrow} alt='' class="mr-10 rotate-270" />
                              Go to Product dashboard
                            </Dropdown.Item>
                          </Dropdown.Menu>
                        </Dropdown>}
              </div>
              }
              </div>
              {!this.product && this.props.showBreadCrumb && this.renderProjectName()}
              {this.renderBackOption()}
            </div>
            <div className="d-flex">
              {/* {this.props.showFunctionTabs && <NavbarTabs {...this.props}></NavbarTabs> } */}
              {/* {this.props.showFunctionTabs && <div>Navbar 2</div> } */}
              {/* {this.props.billingsNavTabs && < BillingNavBar {...this.props}/>} */}
              {/* {this.props.billingsNavTabs && <div>Billing 2</div>} */}
              {this.renderManageNavbarHeading()}
            </div>
            <div className="d-flex justify-content-end logoContainer">
              {/* global search */}
              {this.renderSearchDropdown()}
              
            
              {/* user profile */}
              <UserProfile {...this.props}></UserProfile>
            </div>
          </div>
        </div>
        {/* {this.renderNavigationTabs()} */}
      </React.Fragment>
    );
  }
}

export default NavBar;
