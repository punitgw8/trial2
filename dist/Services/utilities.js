"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
exports.extractFirstOrg = extractFirstOrg;
exports.getConsumedServices = getConsumedServices;

function extractFirstOrg(organizations) {
  if (organizations && Object.keys(organizations).length > 0) {
    const firstOrgId = Object.keys(organizations)[0];
    return organizations[firstOrgId];
  }

  return {};
}

function getConsumedServices(organizations, orgId) {
  const organization = orgId ? organizations[orgId] : extractFirstOrg(organizations);
  let consumedServices = organization.consumed_services || [];
  return consumedServices;
}

var _default = {
  extractFirstOrg,
  getConsumedServices
};
exports.default = _default;