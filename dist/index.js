"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "NavBar", {
  enumerable: true,
  get: function get() {
    return _header.default;
  }
});
Object.defineProperty(exports, "Trial", {
  enumerable: true,
  get: function get() {
    return _trial.default;
  }
});

var _header = _interopRequireDefault(require("./components/header"));

var _trial = _interopRequireDefault(require("./components/trial"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }