"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

require("core-js/modules/web.dom-collections.iterator.js");

require("core-js/modules/es.regexp.exec.js");

require("core-js/modules/es.string.search.js");

require("core-js/modules/es.array.sort.js");

require("core-js/modules/es.string.includes.js");

require("core-js/modules/es.string.split.js");

var _react = _interopRequireWildcard(require("react"));

var _authService = require("../auth/authService");

var _reactBootstrap = require("react-bootstrap");

var _reactRedux = require("react-redux");

var _reactAvatar = _interopRequireDefault(require("react-avatar"));

var _lightArrow = _interopRequireDefault(require("../assets/images/icons/light-arrow.svg"));

var _reactWindow = require("react-window");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

const mapStateToProps = state => {
  return {
    profile: state.profile
  };
};

class UserProfile extends _react.Component {
  constructor() {
    super(...arguments);

    _defineProperty(this, "state", {
      firstName: "",
      lastName: "",
      email: "",
      orgFilter: "",
      moreFlag: false
    });
  }

  getPreviousUser() {
    // return localStorage.getItem(PREVIOUS_USER);
    return "abcd";
  }

  returnName() {
    let firstName = this.props.profile.first_name || "";
    let name = firstName;
    let email = this.props.profile.email;

    if (name === " ") {
      return email;
    } else {
      return name;
    }
  }

  openAccountAndSettings() {
    this.props.history.push({
      pathname: "/orgs/".concat(this.props.organizationId, "/manage"),
      search: this.props.location.search
    });
  }

  exploreProducts() {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-item",
      onClick: () => this.props.history.push({
        pathname: "/orgs/".concat(this.props.organizationId, "/products")
      })
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: "settings-label"
    }, /*#__PURE__*/_react.default.createElement("img", {
      src: process.env.REACT_APP_SOCKET_ICON,
      alt: "sokt-icon",
      height: "18px",
      width: "18px",
      className: "mr-10"
    }), "Explore Products"));
  }

  renderBilling() {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-item",
      onClick: () => this.props.history.push({
        pathname: "/orgs/".concat(this.props.organizationId, "/billing")
      })
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: "settings-label"
    }, /*#__PURE__*/_react.default.createElement("svg", {
      width: "18",
      height: "18",
      viewBox: "0 0 18 18",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, /*#__PURE__*/_react.default.createElement("path", {
      d: "M9.75 1.5H4.5C4.10217 1.5 3.72064 1.65804 3.43934 1.93934C3.15804 2.22064 3 2.60218 3 3V15C3 15.3978 3.15804 15.7794 3.43934 16.0607C3.72064 16.342 4.10217 16.5 4.5 16.5H13.5C13.8978 16.5 14.2794 16.342 14.5607 16.0607C14.842 15.7794 15 15.3978 15 15V6.75L9.75 1.5Z",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M9.75 1.5V6.75H15",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M8.2983 14.7273H8.96875L8.97443 14.071C10.2045 13.9773 10.9176 13.3239 10.9205 12.3381C10.9176 11.3693 10.1875 10.8551 9.17614 10.6278L9.00852 10.5881L9.01989 9.16761C9.39773 9.25568 9.62784 9.49716 9.66193 9.85511H10.8409C10.8267 8.91477 10.125 8.24148 9.03125 8.12216L9.03693 7.45455H8.36648L8.3608 8.11648C7.25 8.22443 6.46591 8.89489 6.47159 9.86364C6.46875 10.7216 7.07386 11.2131 8.05682 11.4489L8.32955 11.517L8.31534 13.0199C7.85227 12.9318 7.53977 12.6477 7.50852 12.1733H6.31818C6.34659 13.321 7.09943 13.9631 8.30398 14.0682L8.2983 14.7273ZM8.9858 13.0199L8.99716 11.6932C9.4375 11.8324 9.67614 12.0114 9.67898 12.3352C9.67614 12.679 9.41477 12.9347 8.9858 13.0199ZM8.33807 10.4148C7.98295 10.2926 7.72727 10.108 7.73295 9.78125C7.73295 9.47727 7.94886 9.24148 8.34943 9.15909L8.33807 10.4148Z",
      fill: "black"
    })), "Billing"));
  }

  renderSettings() {
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-item",
      onClick: () => this.props.history.push({
        pathname: "/orgs/".concat(this.props.organizationId, "/manage/authkeys")
      })
    }, /*#__PURE__*/_react.default.createElement("span", {
      className: "settings-label"
    }, /*#__PURE__*/_react.default.createElement("svg", {
      width: "18",
      height: "18",
      viewBox: "0 0 18 18",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, /*#__PURE__*/_react.default.createElement("path", {
      d: "M9 11.25C10.2426 11.25 11.25 10.2426 11.25 9C11.25 7.75736 10.2426 6.75 9 6.75C7.75736 6.75 6.75 7.75736 6.75 9C6.75 10.2426 7.75736 11.25 9 11.25Z",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M14.55 11.25C14.4502 11.4762 14.4204 11.7271 14.4645 11.9704C14.5086 12.2137 14.6246 12.4382 14.7975 12.615L14.8425 12.66C14.982 12.7993 15.0926 12.9647 15.1681 13.1468C15.2436 13.3289 15.2824 13.5241 15.2824 13.7212C15.2824 13.9184 15.2436 14.1136 15.1681 14.2957C15.0926 14.4778 14.982 14.6432 14.8425 14.7825C14.7032 14.922 14.5378 15.0326 14.3557 15.1081C14.1736 15.1836 13.9784 15.2224 13.7812 15.2224C13.5841 15.2224 13.3889 15.1836 13.2068 15.1081C13.0247 15.0326 12.8593 14.922 12.72 14.7825L12.675 14.7375C12.4982 14.5646 12.2737 14.4486 12.0304 14.4045C11.7871 14.3604 11.5362 14.3902 11.31 14.49C11.0882 14.5851 10.899 14.7429 10.7657 14.9441C10.6325 15.1454 10.561 15.3812 10.56 15.6225V15.75C10.56 16.1478 10.402 16.5294 10.1207 16.8107C9.83935 17.092 9.45782 17.25 9.06 17.25C8.66217 17.25 8.28064 17.092 7.99934 16.8107C7.71803 16.5294 7.56 16.1478 7.56 15.75V15.6825C7.55419 15.4343 7.47384 15.1935 7.32938 14.9915C7.18493 14.7896 6.98305 14.6357 6.75 14.55C6.52379 14.4502 6.27285 14.4204 6.02956 14.4645C5.78626 14.5086 5.56176 14.6246 5.385 14.7975L5.34 14.8425C5.20069 14.982 5.03526 15.0926 4.85316 15.1681C4.67106 15.2436 4.47587 15.2824 4.27875 15.2824C4.08163 15.2824 3.88644 15.2436 3.70434 15.1681C3.52224 15.0926 3.35681 14.982 3.2175 14.8425C3.07803 14.7032 2.9674 14.5378 2.89191 14.3557C2.81642 14.1736 2.77757 13.9784 2.77757 13.7812C2.77757 13.5841 2.81642 13.3889 2.89191 13.2068C2.9674 13.0247 3.07803 12.8593 3.2175 12.72L3.2625 12.675C3.4354 12.4982 3.55139 12.2737 3.5955 12.0304C3.63962 11.7871 3.60984 11.5362 3.51 11.31C3.41493 11.0882 3.25707 10.899 3.05585 10.7657C2.85463 10.6325 2.61884 10.561 2.3775 10.56H2.25C1.85217 10.56 1.47064 10.402 1.18934 10.1207C0.908035 9.83935 0.75 9.45782 0.75 9.06C0.75 8.66217 0.908035 8.28064 1.18934 7.99934C1.47064 7.71803 1.85217 7.56 2.25 7.56H2.3175C2.56575 7.55419 2.8065 7.47384 3.00847 7.32938C3.21044 7.18493 3.36429 6.98305 3.45 6.75C3.54984 6.52379 3.57962 6.27285 3.5355 6.02956C3.49139 5.78626 3.3754 5.56176 3.2025 5.385L3.1575 5.34C3.01803 5.20069 2.9074 5.03526 2.83191 4.85316C2.75642 4.67106 2.71757 4.47587 2.71757 4.27875C2.71757 4.08163 2.75642 3.88644 2.83191 3.70434C2.9074 3.52224 3.01803 3.35681 3.1575 3.2175C3.29681 3.07803 3.46224 2.9674 3.64434 2.89191C3.82644 2.81642 4.02163 2.77757 4.21875 2.77757C4.41587 2.77757 4.61106 2.81642 4.79316 2.89191C4.97526 2.9674 5.14069 3.07803 5.28 3.2175L5.325 3.2625C5.50176 3.4354 5.72626 3.55139 5.96956 3.5955C6.21285 3.63962 6.46379 3.60984 6.69 3.51H6.75C6.97183 3.41493 7.16101 3.25707 7.29427 3.05585C7.42752 2.85463 7.49904 2.61884 7.5 2.3775V2.25C7.5 1.85217 7.65803 1.47064 7.93934 1.18934C8.22064 0.908035 8.60217 0.75 9 0.75C9.39782 0.75 9.77935 0.908035 10.0607 1.18934C10.342 1.47064 10.5 1.85217 10.5 2.25V2.3175C10.501 2.55884 10.5725 2.79463 10.7057 2.99585C10.839 3.19707 11.0282 3.35493 11.25 3.45C11.4762 3.54984 11.7271 3.57962 11.9704 3.5355C12.2137 3.49139 12.4382 3.3754 12.615 3.2025L12.66 3.1575C12.7993 3.01803 12.9647 2.9074 13.1468 2.83191C13.3289 2.75642 13.5241 2.71757 13.7212 2.71757C13.9184 2.71757 14.1136 2.75642 14.2957 2.83191C14.4778 2.9074 14.6432 3.01803 14.7825 3.1575C14.922 3.29681 15.0326 3.46224 15.1081 3.64434C15.1836 3.82644 15.2224 4.02163 15.2224 4.21875C15.2224 4.41587 15.1836 4.61106 15.1081 4.79316C15.0326 4.97526 14.922 5.14069 14.7825 5.28L14.7375 5.325C14.5646 5.50176 14.4486 5.72626 14.4045 5.96956C14.3604 6.21285 14.3902 6.46379 14.49 6.69V6.75C14.5851 6.97183 14.7429 7.16101 14.9441 7.29427C15.1454 7.42752 15.3812 7.49904 15.6225 7.5H15.75C16.1478 7.5 16.5294 7.65803 16.8107 7.93934C17.092 8.22064 17.25 8.60217 17.25 9C17.25 9.39782 17.092 9.77935 16.8107 10.0607C16.5294 10.342 16.1478 10.5 15.75 10.5H15.6825C15.4412 10.501 15.2054 10.5725 15.0041 10.7057C14.8029 10.839 14.6451 11.0282 14.55 11.25V11.25Z",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    })), "Auth Keys"));
  }

  getCurrentOrg() {
    return this.props.organizations[this.props.organizationId];
  }

  renderAvatarWithOrg(_onClick, ref1) {
    const currentOrg = this.getCurrentOrg();
    return /*#__PURE__*/_react.default.createElement("div", {
      className: "profile-box",
      onClick: e => {
        e.preventDefault();

        _onClick(e);
      }
    }, /*#__PURE__*/_react.default.createElement(_reactAvatar.default, {
      color: '#343a40',
      name: this.returnName(),
      size: 24,
      round: "30px"
    }), /*#__PURE__*/_react.default.createElement("span", {
      className: "px-3 org-name"
    }, currentOrg.name), /*#__PURE__*/_react.default.createElement("img", {
      ref: ref1,
      src: _lightArrow.default,
      alt: "settings-gear",
      className: "transition"
    }));
  }

  getUserDetails() {
    let firstName = this.props.profile.first_name || "";
    let lastName = this.props.profile.last_name || "";
    let name = firstName + " " + lastName;
    let email = this.props.profile.email;
    return {
      email,
      name
    };
  }

  getAllOrgs() {
    const orgsArray = Object.values(this.props.organizations || {});
    const {
      orgFilter
    } = this.state;
    const filteredOrgsArray = orgsArray.filter(org => org.name.toLowerCase().includes(orgFilter.toLowerCase())).sort((a, b) => {
      if (a.name.toLowerCase() < b.name.toLowerCase()) return -1;else if (a.name.toLowerCase() > b.name.toLowerCase()) return 1;else return 0;
    });
    return filteredOrgsArray;
  }

  setShowFlag() {
    let orgFilter = this.state.orgFilter;
    let moreFlag = !this.state.moreFlag;

    if (!moreFlag) {
      orgFilter = "";
    }

    this.setState({
      orgFilter,
      moreFlag
    });
  }

  setOrgFilter(orgFilter) {
    this.setState({
      orgFilter
    });
  }

  getItemCount(orgCount) {
    let showFlag = this.state.moreFlag;

    if (orgCount > 5 && !showFlag) {
      return 5;
    } else {
      return orgCount;
    }
  }

  switchOrgRoute(id) {
    let pathname = "";
    let pathArray = this.props.history.location.pathname.split('/');

    if (pathArray[3] === 'manage') {
      pathname = "/orgs/".concat(id, "/manage");
    } else {
      pathname = "/orgs/".concat(id);
    }

    this.props.history.push({
      pathname
    });
  }

  renderOrgList() {
    const orgsLength = Object.keys(this.props.organizations || {}).length;
    let filteredOrgsArray = this.getAllOrgs();
    filteredOrgsArray = filteredOrgsArray.filter(org => org.id !== this.props.organizationId);

    const orgItem = _ref => {
      let {
        index,
        style
      } = _ref;
      const item = filteredOrgsArray[index];
      return /*#__PURE__*/_react.default.createElement("div", {
        key: item.id,
        className: "d-flex align-items-center p-2 settings-item justify-space-between",
        style: style,
        onClick: () => {
          this.switchOrgRoute(item.id);
          this.props.switch_org(item.id);
        }
      }, /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, null, /*#__PURE__*/_react.default.createElement("div", {
        class: "d-flex justify-space-between"
      }, /*#__PURE__*/_react.default.createElement("div", {
        className: "body-3 text-black text-truncate pl-2"
      }, item.name), /*#__PURE__*/_react.default.createElement("div", {
        className: "orgs-icon"
      }, /*#__PURE__*/_react.default.createElement("svg", {
        width: "18",
        height: "18",
        viewBox: "0 0 18 18",
        fill: "none",
        xmlns: "http://www.w3.org/2000/svg"
      }, /*#__PURE__*/_react.default.createElement("path", {
        d: "M6.75 13.5L11.25 9L6.75 4.5",
        stroke: "#4F4F4F",
        "stroke-width": "1.5",
        "stroke-linecap": "round",
        "stroke-linejoin": "round"
      }))))));
    };

    return orgsLength > 1 && /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("div", {
      className: "text-uppercase text-sm-bold"
    }, "SWITCH ORGS"), /*#__PURE__*/_react.default.createElement("div", {
      className: "orgs-list profile-sm-block"
    }, this.state.moreFlag && /*#__PURE__*/_react.default.createElement("div", null, /*#__PURE__*/_react.default.createElement("input", {
      className: "form-control",
      onChange: e => this.setOrgFilter(e.target.value, filteredOrgsArray.length || 0),
      value: this.state.orgFilter,
      placeholder: "Search"
    })), filteredOrgsArray.length == 0 ? /*#__PURE__*/_react.default.createElement("div", {
      className: "pb-2 text-center w-100"
    }, /*#__PURE__*/_react.default.createElement("small", {
      className: "body-6"
    }, "No Organizations Found")) : /*#__PURE__*/_react.default.createElement(_reactWindow.FixedSizeList, {
      height: filteredOrgsArray.length < 5 ? 36 * filteredOrgsArray.length : 220,
      itemCount: this.getItemCount(filteredOrgsArray.length),
      itemSize: 44
    }, orgItem)), orgsLength > 5 && /*#__PURE__*/_react.default.createElement("div", {
      className: "ShowMore text-center",
      onClick: () => this.setShowFlag()
    }, !this.state.moreFlag ? "Show more" : "Show less"));
  }

  render() {
    return /*#__PURE__*/_react.default.createElement(_reactBootstrap.Nav, null, (0, _authService.getCurrentUser)() === null ? /*#__PURE__*/_react.default.createElement("div", {
      id: "sokt-sso",
      "data-redirect-uri": process.env.REACT_APP_UI_BASE_URL,
      "data-source": "sokt-app",
      "data-token-key": "sokt_at",
      "data-view": "button"
    }) :
    /*#__PURE__*/
    // <Navbar.Collapse className="justify-content-end">
    _react.default.createElement(_react.default.Fragment, null, /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown, {
      drop: "down",
      className: "profile-dropdown transition d-flex align-items-center"
    }, /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Toggle, {
      as: /*#__PURE__*/_react.default.forwardRef((_ref2, ref1) => {
        let {
          children,
          onClick
        } = _ref2;
        return this.renderAvatarWithOrg(onClick, ref1);
      }),
      id: "dropdown-custom-components"
    }), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Menu, {
      className: "settings-content"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "text-center org-name"
    }, this.getCurrentOrg().name || null), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Divider, null), /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-item hover-block pt-0 pb-0",
      onClick: () => {}
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-label d-flex align-items-center"
    }, /*#__PURE__*/_react.default.createElement("svg", {
      width: "25",
      height: "25",
      viewBox: "0 0 18 18",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, /*#__PURE__*/_react.default.createElement("path", {
      d: "M15 15.75V14.25C15 13.4544 14.6839 12.6913 14.1213 12.1287C13.5587 11.5661 12.7956 11.25 12 11.25H6C5.20435 11.25 4.44129 11.5661 3.87868 12.1287C3.31607 12.6913 3 13.4544 3 14.25V15.75",
      stroke: "#000",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M9 8.25C10.6569 8.25 12 6.90685 12 5.25C12 3.59315 10.6569 2.25 9 2.25C7.34315 2.25 6 3.59315 6 5.25C6 6.90685 7.34315 8.25 9 8.25Z",
      stroke: "#000",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    })), /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-user-name"
    }, /*#__PURE__*/_react.default.createElement("span", {
      class: "settings-label"
    }, this.getUserDetails().name), /*#__PURE__*/_react.default.createElement("span", {
      class: "settings-label-light"
    }, this.getUserDetails().email)))), /*#__PURE__*/_react.default.createElement("div", {
      className: "profile-sm-block"
    }, /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-item",
      onClick: () => {
        this.openAccountAndSettings();
      }
    }, /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, null, /*#__PURE__*/_react.default.createElement("span", {
      className: "settings-label"
    }, /*#__PURE__*/_react.default.createElement("svg", {
      width: "18",
      height: "18",
      viewBox: "0 0 18 18",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, /*#__PURE__*/_react.default.createElement("path", {
      d: "M12 15.75V14.25C12 13.4544 11.6839 12.6913 11.1213 12.1287C10.5587 11.5661 9.79565 11.25 9 11.25H3.75C2.95435 11.25 2.19129 11.5661 1.62868 12.1287C1.06607 12.6913 0.75 13.4544 0.75 14.25V15.75",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M6.375 8.25C8.03185 8.25 9.375 6.90686 9.375 5.25C9.375 3.59315 8.03185 2.25 6.375 2.25C4.71815 2.25 3.375 3.59315 3.375 5.25C3.375 6.90686 4.71815 8.25 6.375 8.25Z",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M15 6V10.5",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M17.25 8.25H12.75",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    })), "Invite Team"))), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, null, this.renderBilling()), /*#__PURE__*/_react.default.createElement(_reactBootstrap.Dropdown.Item, null, this.renderSettings()), /*#__PURE__*/_react.default.createElement("div", {
      className: "settings-item",
      onClick: () => {
        window.location = "/logout";
      }
    }, /*#__PURE__*/_react.default.createElement("span", {
      class: "settings-label"
    }, /*#__PURE__*/_react.default.createElement("svg", {
      width: "18",
      height: "18",
      viewBox: "0 0 18 18",
      fill: "none",
      xmlns: "http://www.w3.org/2000/svg"
    }, /*#__PURE__*/_react.default.createElement("path", {
      d: "M13.7698 4.98047C14.7136 5.92456 15.3563 7.1273 15.6166 8.43661C15.8768 9.74592 15.743 11.103 15.2321 12.3363C14.7211 13.5696 13.8559 14.6236 12.746 15.3652C11.636 16.1068 10.331 16.5027 8.9961 16.5027C7.66117 16.5027 6.35621 16.1068 5.24623 15.3652C4.13624 14.6236 3.27108 13.5696 2.76012 12.3363C2.24916 11.103 2.11536 9.74592 2.37563 8.43661C2.63591 7.1273 3.27856 5.92456 4.22235 4.98047",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    }), /*#__PURE__*/_react.default.createElement("path", {
      d: "M9 1.5V9",
      stroke: "black",
      "stroke-width": "1.5",
      "stroke-linecap": "round",
      "stroke-linejoin": "round"
    })), "Logout"))), this.renderOrgList()))) // </Navbar.Collapse>
    );
  }

}

var _default = (0, _reactRedux.connect)(mapStateToProps, null)(UserProfile);

exports.default = _default;